import './App.css';
import React, { useState, useEffect } from "react"
import { Container, Row, Col } from "reactstrap"
//import styled from "styled-components"

import almacen from "./Almacen.json"
import Productos from "./Productos.jsx"
import Ticket from "./Ticket.jsx"

let unidadesNew = [];
const CATALOGLENGTH = almacen.length;

function App() {
  const [compra, setCompra] = useState([]);
  const [unidades, setUnidades] = useState(unidadesNew);

  useEffect(() => {
    for (let i = 0; i < CATALOGLENGTH; i++) {
      unidadesNew.push(0);
    }
  }, [])


  const Afegir = (fruta) => {
    //console.log(fruta.nom + " comprat");
    setCompra(() => {
      let nuevaCompra = [...compra];
      nuevaCompra.push(fruta.id);
      return (nuevaCompra)
    });
    
  }

  useEffect(() => {
    //console.log(compra[compra.length - 1])
    const lastFrutaComprada = compra[compra.length - 1] - 1;
    let unidadesEdit = [...unidades];
    unidadesEdit[lastFrutaComprada]++;
    setUnidades(unidadesEdit);
  }, [compra])

  return (
    <Container>
      <Row>
        <Col>
          <h1 style={{ textAlign: "center" }}>Fruteria</h1>
        </Col>
      </Row>
      <Row>
        <Col sm={{ size: 6 }} >
          <Productos afegir={Afegir} inventario={almacen} />
        </Col>
        <Col sm={{ size: 6 }} >
          <Ticket inventario={almacen} lista={compra} unidades={unidades} />
        </Col>
      </Row>
    </Container>
  );
}

export default App;
