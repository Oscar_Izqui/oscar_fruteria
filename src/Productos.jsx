import React from "react"
import styled from "styled-components"
import { Button } from "reactstrap"

const Productos = styled.div`
  background-color: green;
  color: white;
  width: 100%;
  height: 60px;
  line-height: 55px;
  padding: 0px 15px;
  margin-bottom: 10px;
`;

const Catalogo = (props) => {

    const productos = props.inventario.map((fruta) => {
        return (
            <Productos key={fruta.id}>
                {fruta.nom} ({fruta.preu} €/u)
                <Button onClick={() => props.afegir(fruta)}>
                    Afegir
                </Button>
            </Productos>
        )
    });

    return (
        <>
            {productos}
        </>
    );
};

export default Catalogo;