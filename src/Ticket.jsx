import React/* , { useState, useEffect } */ from "react"
import styled from "styled-components"

/* const Ticket = styled.div`
  background-color: red;
  width: 100%;
  height: 170px;
`; */
const Productos = styled.div`
  background-color: green;
  color: white;
  width: 100%;
  height: 60px;
  line-height: 55px;
  padding: 0px 15px;
  margin-bottom: 10px;
`;

const Total = styled.div`
    width: 100%;
    height: 60px;
    margin-top: 10px;
    border: solid black 2px;
    line-height: 55px;
    padding: 0px 15px;
`;

/* let unidades = []; */

const Ticket = (props) => {

    /* const catalogoLength = props.inventario.length;
    useEffect(() => {
        for (let i = 0; i < catalogoLength; i++ ){
            unidades.push(0);
        }
    }, []) */

    console.log(props.unidades); //Esto funciona raro



    const productos = props.inventario.map((fruta) => {
        if (props.lista.includes(fruta.id)) {
            return (
                <Productos key={fruta.id}>
                    {fruta.nom} {props.unidades[fruta.id - 1]} unitats
                </Productos>
            )
        }
    });

    return (
        <>
            {productos}
            <Total>Total: 0€</Total>
        </>
    );
};

export default Ticket;